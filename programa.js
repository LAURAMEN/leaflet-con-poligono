// Crear un objeto  (variables compuestas) para representar el valor


//L representa a la biblioteca leaflet


let miMapa = L.map('mapid');

//definir caracteristcas del visor 

miMapa.setView([4.710989,-74.072090], 16);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(miMapa);

//crear un objeto marcador 
var miMarcador = L.marker([4.746556, -74.101162]);

miMarcador.addTo(miMapa)

//Json
let circle = L.circle([4.748203,-74.102084], {
    color: 'blue',
    fillColor: 'yellow',
    fillOpacity: 0.5,
    radius: 500
});

circle.addTo(miMapa)

//poligono

let polygon = L.polygon([
    [
      4.747833730281452,  
      -74.10061061382294],
      [
        4.747518315575633,
        -74.10076081752777],
      [
        4.7472884369703765,
        -74.10080373287201],
      [
        4.747085288371756,
        -74.0995055437088],
      [
        4.746245963264403,
        -74.09964501857758],
      [
        4.746245963264403,
        -74.09934997558594],
      [
        4.746785911380534,
        -74.09932315349579],
      [
        4.746711067112433,
        -74.09832000732422],
      [
        4.747833730281452,
        -74.10061061382294]
]).addTo(miMapa);